import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import "../../styles/Auth/login.css";
import Logo from "../../assets/logo-primary.png";
import { useHistory } from "react-router-dom";
function Signup() {
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    dispatch({ type: "LOGIN_SUCCESS" });
  }, []);
  const handleSwitchPage = () => {
    history.push("/");
  };
  return (
    <div>
      <div className="limiter">
        <div className="container-login100">
          <div className="wrap-login100">
            <div className="login100-form validate-form">
              <span className="login100-form-title p-b-48">
                <img src={Logo} style={{ width: "120px", height: "120px" }} />
              </span>

              <div
                className="wrap-input100 validate-input"
                data-validate="Enter Name"
              >
                <input className="input100" type="text" name="name" />
                <span className="focus-input100" data-placeholder="name"></span>
              </div>

              <div
                className="wrap-input100 validate-input"
                data-validate="Valid email is: a@b.c"
              >
                <input className="input100" type="text" name="email" />
                <span
                  className="focus-input100"
                  data-placeholder="Email"
                ></span>
              </div>

              <div
                className="wrap-input100 validate-input"
                data-validate="Enter password"
              >
                <span className="btn-show-pass"></span>
                <input className="input100" type="password" name="pass" />
                <span
                  className="focus-input100"
                  data-placeholder="Password"
                ></span>
              </div>

              <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                  <div className="login100-form-bgbtn"></div>
                  <button className="login100-form-btn">Signup</button>
                </div>
              </div>

              <div
                className="text-center p-t-115"
                style={{ textAlign: "center", paddingTop: "20px" }}
              >
                <span className="txt1">Already have an account?</span>

                <p className="txt2" onClick={() => handleSwitchPage()}>
                  Login
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Signup;
